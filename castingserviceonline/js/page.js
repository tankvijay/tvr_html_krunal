$(document).ready(function () {

    $(".app-scroll-container").mCustomScrollbar();

    $(".side-menu .has-submenu").click(function () {
        $(this).next().slideToggle();
    })

    $("header .menu-toggle-btn").click(function () {
        $("body").addClass("sidemenu-expand");
    });

    $("header .responsive-menu .menu-close-btn").click(function () {
        $("body").removeClass("sidemenu-expand");
    });

    $("#profileVisibilityBtn").click(function () {
        $(this).toggleClass("visibility-off");
    })
    /*----------star rating---------*/
    if ($(".star-review").length) {
        $(".star-review").rating({
            "value": 5,
            "click": function (e) {
                if (e.event != undefined) {
                    var targetEle = e.event.target;
                    $(targetEle).parents(".star-rating").find(".rating").text(e.stars + ".0");
                }
            }
        });
    }

    /*---------multit select plugin --------*/
    function format(option) {
        var image = $(option.element).attr("dataimg");
        if (!option.id) { return option.text; }
        var ob = '<img src="images/' + image + '" /><span>' + option.text + '</span>';	// replace image source with option.img (available in JSON)
        return ob;
    };

    if ($(".cust-multiselect").length) {
        $(".cust-multiselect").select2({
            placeholder: "Select something!!",
            width: "100%",
            allowClear: true,
            templateResult: format,
            templateSelection: function (option) {
                var image = $(option.element).attr("dataimg");
                if (option.id.length > 0) {
                    return '<img src="images/' + image + '" /><span>' + option.text + '</span>';
                } else {
                    return option.text;
                }
            },
            escapeMarkup: function (m) {
                return m;
            }
        });
    }

    frontHeaderScrollPos();

    //all scroll related code
    $(window).scroll(function (event) {
        frontHeaderScrollPos();
    });

    //date time picker
    if (window.jQuery().datetimepicker) {
        $('.datetimepicker').datetimepicker({
            // Formats
            // follow MomentJS docs: https://momentjs.com/docs/#/displaying/format/
            format: 'DD-MM-YYYY hh:mm A',

            // Your Icons
            // as Bootstrap 4 is not using Glyphicons anymore
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-check',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        });
    }

    //text expand collapse
    $(".expCollap-text .action-btn").click(function () {
        if ($(this).parents(".expCollap-text").hasClass('collasped')) {
            $(this).parents(".expCollap-text").removeClass('collasped');
            $(this).find('span').text("Show less");
            $(this).find('button i').removeClass("fa-plus").addClass('fa-minus');
        } else {
            $(this).parents(".expCollap-text").addClass('collasped');
            $(this).find('span').text("Show more");
            $(this).find('button i').addClass("fa-plus").removeClass('fa-minus');
        }
    });

    if($(".upload-list .media .btn").length ){
        $(".upload-list .media .btn").click(function(){
            $(this).parents(".list-group-item").fadeOut();
        })
    }

    //like button toggle
    $(".like-btn").click(function(){
        $(this).toggleClass("liked");
    });

   // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
    // var previewNode = document.querySelector("#template");
    // previewNode.id = "";
    // var previewTemplate = previewNode.parentNode.innerHTML;
    // previewNode.parentNode.removeChild(previewNode);
    var myDropzone
    if($(".dropzone").length > 0){
        myDropzone = new Dropzone(document.body, { 
            url: "/", 
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            // previewTemplate: previewTemplate,
            autoQueue: false, 
            previewsContainer: "#previews",
            clickable: ".upload-link",
            addedfile: function(e){
                var cardHtml = '';
                cardHtml = '<div class="card-outer"><div  class="card app-card upload-item-card">'+
                '<div class="card-body d-flex p-3">'+
                    '<div class="left-box mr-auto">'+
                        '<h4 class="item-title">1. Audio Books</h4>'+
                        '<p class="file-name" data-dz-name title="'+e.name+'">'+e.name+'</p>'+
                        '<p class="file-count">0976566797987</p>'+
                    '</div>'+
                    '<div class="right-box">'+
                        '<div class="music-player" id="musicPlayer">'+
                            '<div class="ui360">'+
                                '<a href="https://www.soundjay.com/nature/sounds/rain-01.mp3"></a>'+
                            '</div>'+
                        '</div>'+
                        '<button data-dz-remove class="btn app-button-clear close-btn abs-right-top">'+
                            '<i class="icon-close text-red-color"></i>'+
                        '</button>'+
                        '<button  class="btn app-button-clear edit-btn abs-right-bottom">'+
                            '<img src="images/icon-blue-edit1.png" alt="icon" />'+
                        '</button>'+
                    '</div>'+
                '</div>'+
            '</div></div>';
            $('#previews').append(cardHtml);
            threeSixtyPlayer.init();
            }
        });
    }

    $( "body" ).on( "click", ".dropzone .upload-item-card .close-btn", function() {
        $( this ).parents('.card-outer').remove();
    });

    if($('.owl-carousel').length > 0){
        $('.owl-carousel').owlCarousel({
            loop:true,
            nav:true,
            items: 1
        })
    }

});

function frontHeaderScrollPos() {
    var scrollTop = $(this).scrollTop();
    if (scrollTop > 0) {
        $(".main-header").addClass("page-scrolled");
    } else {
        $(".main-header").removeClass("page-scrolled");
    }
}
